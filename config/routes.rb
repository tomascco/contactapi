# frozen_string_literal: true

Rails.application.routes.draw do
  devise_for :users
  namespace :api do
    namespace :v1 do
      resources :contacts
    end
  end

  namespace :api do
    namespace :v2 do
      resources :contacts do
        resources :addresses
      end
    end
  end

  root to: 'application#health_check'

  # constraints subdomain: 'api' do
  #   scope module: 'api' do
  #     namespace :v1 do
  #       resources :contacts
  #     end
  #   end
  # end
end
