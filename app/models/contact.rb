# frozen_string_literal: true

class Contact < ApplicationRecord
  belongs_to :user
  has_many :addresses
  validates :name, :user, presence: true
  scope :page, ->(n) { limit(10).offset(n * 10) }
end
