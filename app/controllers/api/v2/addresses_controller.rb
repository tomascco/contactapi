# frozen_string_literal: true

class Api::V2::AddressesController < Api::V2::ApiController
  before_action :set_contact
  before_action :set_address, only: %i[show update destroy]
  before_action :require_authorization!, only: %i[show update destroy]

  # GET /api/v2/:contact_id/addresses
  def index
    @addresses = @contact.addresses
    render json: @addresses
  end

  # GET /api/v2/:contact_id/:id
  def show
    render json: @address
  end

  # POST /api/v2/:contact_id/address
  def create
    @address = @contact.addresses.build(address_params)
    if @address.save
      render json: @address, status: :created
    else
      render json: @address.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /api/v2/:contact_id/:id
  def update
    if @address.update(address_params)
      render json: @address
    else
      render json: @address.errors, status: :unprocessable_entity
    end
  end

  # DELETE /api/v2/:contact_id/:id
  def destroy
    @address.destroy
  end

  private

  # Use callbacks to share common setup or constraints between actions.

  def set_contact
    @contact = Contact.find(params[:contact_id])
  end

  def set_address
    @address = @contact.addresses.find(params[:id])
  end

  def address_params
    params.require(:address).permit(:street, :number, :neighborhood, :city, :state)
  end

  def require_authorization!
    render json: {}, status: :forbidden unless current_user == @contact.user
  end
end
