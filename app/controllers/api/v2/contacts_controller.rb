# frozen_string_literal: true

class Api::V2::ContactsController < Api::V2::ApiController
  before_action :set_contact, only: %i[show update destroy]
  before_action :require_authorization!, only: %i[show update destroy]

  # GET /api/v2/contacts
  def index
    page = params[:page].to_i
    @contacts = current_user.contacts.page(page)
    render json: @contacts
  end

  # GET /api/v2/contacts/1
  def show
    render json: @contact
  end

  # POST /api/v2/contacts
  def create
    @contact = Contact.new(contact_params.merge(user: current_user))
    if @contact.save
      render json: @contact, status: :created
    else
      render json: @contact.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /api/v2/contacts/1
  def update
    if @contact.update(contact_params)
      render json: @contact
    else
      render json: @contact.errors, status: :unprocessable_entity
    end
  end

  # DELETE /api/v2/contacts/1
  def destroy
    @contact.destroy
  end

  private

  # Use callbacks to share common setup or constraints between actions.

  def set_contact
    @contact = Contact.find(params[:id])
  end

  def contact_params
    params.require(:contact).permit(:name, :email, :phone, :description)
  end

  def require_authorization!
    render json: {}, status: :forbidden unless current_user == @contact.user
  end
end
