# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

100.times do
  Contact.create(
    user: User.first,
    name: Faker::Name.name,
    email: Faker::Internet.email,
    phone: Faker::PhoneNumber.phone_number
  )
end

200.times do
  Address.create(
    street: Faker::Address.street_name,
    number: Faker::Address.building_number,
    neighborhood: 'Tomáslândia',
    city: Faker::Address.city,
    state: Faker::Address.state,
    contact_id: Faker::Number.between(from: 1, to: 100)
  )
end
