FROM ruby:2.6.5

RUN mkdir /app
WORKDIR /app

ENV RAILS_LOG_TO_STDOUT TRUE
ENV RAILS_ENV production
ENV RACK_ENV production

RUN apt-get update \
  && apt-get install -y --no-install-recommends \
  postgresql-client --fix-missing \
  && rm -rf /var/lib/apt/lists/*

COPY Gemfile /app/Gemfile
COPY Gemfile.lock /app/Gemfile.lock

RUN gem install bundler -v 1.17.2

RUN bundle install --deployment

COPY . /app

EXPOSE 8080

CMD ["bundler", "exec", "rails", "s", "--port=8080"]